Texo IT Test - Web
> Simple test suite based on a simple page web.

## Configuration

* To compile the classes and run the tests, you must have Java 8+ JDK installed and PATH correct;
* This project is based on Maven, so you must have it installed on your computer, with the PATH to the bin correctly;
* The path to the chromedriver need to be set too, in my case, he is on the env variables; 
* After this, just go to the source folder where we have the pom.xml and execute the next steps.


## Description

To run the test suite, just need to be in the root folder where is the pom.xml and run the following command:

```sh
mvn test -Dtest="TestRunner"
```

After we run the test suite, the reports will be available at the Cucumber folder report, most precisely the cucumber-pretty.html.


