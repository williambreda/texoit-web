package pages;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class JSONPlaceholderPage {

    protected WebDriver driver;

    public JSONPlaceholderPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 4), this);
    }

    @FindBy (linkText = "/albums/1/photos")
    WebElement linkToPhotos;


    public void accessJsonPlaceholderPage(){
        driver.navigate().to("https://jsonplaceholder.typicode.com/guide");
        Assert.assertEquals(driver.getTitle(), "JSONPlaceholder - Guide");
    }

    public void navigateToAlbuns() {
        linkToPhotos.click();
    }

    public void saveSourcePageAndValidate(String id) throws IOException {
        String page = driver.getPageSource();
        String jsonFile = FileUtils.readFileToString(new File("src/test/resources/json_files/album_" + id + ".json"), StandardCharsets.UTF_8);
        List<String> jsonContent = new ArrayList<>(Arrays.asList(jsonFile.replace("{","").replace("}","").replace("[","").replace("]", "").trim().split(",")));

        for (String s : jsonContent){
            String leftTrim = s.replaceAll("^\\s+", "");
            if(!page.contains(leftTrim)){
                throw new RuntimeException();
            }
        }
    }
}
