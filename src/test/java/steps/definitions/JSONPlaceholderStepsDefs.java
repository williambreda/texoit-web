package steps.definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.JSONPlaceholderPage;
import steps.hooks.Setup;

import java.io.IOException;

public class JSONPlaceholderStepsDefs {

    JSONPlaceholderPage jsonPlaceholderPage = new JSONPlaceholderPage(Setup.getDriver());

    @Given("Launch JSON Placeholder Page")
    public void launch_json_placeholder_page(){
        jsonPlaceholderPage.accessJsonPlaceholderPage();
    }

    @When("Navigate To Albums")
    public void navigate_to_albums(){
        jsonPlaceholderPage.navigateToAlbuns();
    }

    @Then("Save Page And Validate Id {string}")
    public void save_page_and_validate_id(String id) throws IOException {
        jsonPlaceholderPage.saveSourcePageAndValidate(id);

    }
}
