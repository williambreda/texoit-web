package steps.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Setup {

    private static WebDriver driver;

    @Before
    public void setUp(){
        //System.setProperty("webdriver.chrome.driver", "path to where your chromedriver is if not in env variables");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(15));
    }

    @After
    public void tearDown(){
        driver.close();
    }

    public static WebDriver getDriver(){
        return driver;
    }

}
